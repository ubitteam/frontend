import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DCAreaComponent } from './dcarea.component';

describe('DCAreaComponent', () => {
  let component: DCAreaComponent;
  let fixture: ComponentFixture<DCAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DCAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DCAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
