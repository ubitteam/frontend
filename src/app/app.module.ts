import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import  { routes } from './app.router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ContentAreaComponent } from './home/content-area/content-area.component';
import { LoginComponent } from './login/login.component';
import { FooterComponent } from './home/footer/footer.component';
import { RegisterComponent } from './register/register.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DHeaderComponent } from './dashboard/dheader/dheader.component';
import { DFooterComponent } from './dashboard/dfooter/dfooter.component';
import { DCAreaComponent } from './dashboard/dcarea/dcarea.component';
import { OPDComponent } from './dashboard/dcarea/opd/opd.component';
import { PatientComponent} from './dashboard/dcarea/patient/patient.component';
import { DoctorComponent } from './dashboard/dcarea/doctor/doctor.component';
import { AboutComponent } from './dashboard/dcarea/about/about.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ContentAreaComponent,
    LoginComponent,
    FooterComponent,
    RegisterComponent,
    DashboardComponent,
    DHeaderComponent,
    DFooterComponent,
    DCAreaComponent,
      OPDComponent,
      PatientComponent,
      DoctorComponent,
      AboutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routes
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
