import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule} from '@angular/router';

import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
import {DoctorComponent} from './dashboard/dcarea/doctor/doctor.component';
import {PatientComponent} from './dashboard/dcarea/patient/patient.component';
import {OPDComponent} from './dashboard/dcarea/opd/opd.component';
import {LoginComponent} from  './login/login.component';
import {FooterComponent} from  './home/footer/footer.component';
import {RegisterComponent} from './register/register.component';

import {DashboardComponent} from './dashboard/dashboard.component';
import {DCAreaComponent} from './dashboard/dcarea/dcarea.component';
import {DFooterComponent} from './dashboard/dfooter/dfooter.component';
import {DHeaderComponent} from './dashboard/dheader/dheader.component';
import {AboutComponent} from './dashboard/dcarea/about/about.component';

export const router:Routes =[
  {path:'', redirectTo:'/home', pathMatch:'full'},
  {path: 'home', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'footer', component: FooterComponent},
  {path: 'register', component: RegisterComponent},

  {path: 'dashboard', component: DashboardComponent,
    children:[

      {path: 'dcarea', component: DCAreaComponent,
        children:[



      {path: 'doctor', component: DoctorComponent},
{path: 'patient', component: PatientComponent},
{path: 'opd', component: OPDComponent},
          {path: 'about', component: AboutComponent}


        ] }

    ]},



  {path: 'dfooter', component: DFooterComponent},
  {path: 'dheader', component: DHeaderComponent}
];


export  const  routes: ModuleWithProviders = RouterModule.forRoot(router);
