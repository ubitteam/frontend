import { IadFEPage } from './app.po';

describe('iad-fe App', () => {
  let page: IadFEPage;

  beforeEach(() => {
    page = new IadFEPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
